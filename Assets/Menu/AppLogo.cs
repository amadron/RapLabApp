﻿using UnityEngine;
using System.Collections;

public class AppLogo : MonoBehaviour {
    public int state = 0;
    public GameObject[] objectsToHide;
    public GameObject currMenu;
	// Use this for initialization
	void Start () {
	
	}

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Back();
        }
    }

    public void incrementState()
    {
        if (state == 1 && currMenu != null)
            currMenu.SetActive(false);
        state++;
    }
    public void setMenu(GameObject menu)
    {
        currMenu = menu;
        menu.SetActive(true);
        state = 1;
    }

    public void Back()
    {
        if(state == 1)
        {
            if(currMenu != null)
            {
                currMenu.SetActive(false);
                currMenu = null;
            }
            state--;
        }
        if(state > 1)
        {
             foreach(GameObject obj in objectsToHide)
             {
                if(obj != null)
                {
                    obj.SetActive(false);
                }
             }
            if (currMenu != null)
                currMenu.SetActive(true);
            state--;
        }
    }
}
