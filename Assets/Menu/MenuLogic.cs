﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Text;
using System.Xml;
using UnityEngine.UI;
using System;

public class MenuLogic : MonoBehaviour {
    public GameObject scrollPane;
    public string machineName;
    public string contactFile = "Ansprechpartner.txt";
    private string checklistFile;
    private string descriptionFile;
    private string contactText = null;
    private string machineInfo = null;
    public Text contactElement;
    private string contentPath = null;
    private float contentYPos;
    private ScrollRect scrollView = null;
    private GameObject checkList;
    public GameObject checklistTemplate = null;
    public String reservationURL = "http://dma.ag.htwg-konstanz.de/wp/?page_id=5317";
    public String rapLabSiteURL = "http://openinnovationlab.de/";
	// Use this for initialization
	void Start () {

        Debug.Log("Streaming Asset Path: " + Application.streamingAssetsPath);
        init();
    }

	// Update is called once per frame
	void Update () {
	
	}

    public void init()
    {
        contentPath = Application.streamingAssetsPath + "/";
        scrollView = scrollPane.GetComponent<ScrollRect>();
        if(scrollView != null)
        {
            contentYPos = scrollView.content.position.y;
        }
        checkVariables();
        contactFile = contentPath + contactFile;
        checklistFile = contentPath + machineName + "/Checkliste " + machineName + ".txt";
        descriptionFile = contentPath + machineName + "/Beschreibung " + machineName + ".txt";
        checklistTemplate.SetActive(false);
        createContactText();
        createChecklist();
        createMachineInfoText();
    }

    private void checkVariables()
    {
        if(machineName == null ||  checklistTemplate == null || scrollPane == null || contactElement == null || scrollView == null)
        {
            throw new Exception("Not all Variables are set defined");
        }

    }

    /*
    * Open WebSites
    */
    public void openReservation()
    {
        Application.OpenURL(reservationURL);
    }

    public void openRapLabSite()
    {
        Application.OpenURL(rapLabSiteURL);
    }

    /*
     * Information
     */
    public void showChecklist()
    {
        resetContentPos();
        checkList.SetActive(true);
        scrollPane.gameObject.SetActive(true);
    }

    private void resetContentPos()
    {
        scrollView.content.position = new Vector3(scrollView.content.position.x, contentYPos, scrollView.content.position.z);
    }

    private void createChecklist()
    {
        checkList = checklistTemplate.transform.parent.gameObject;
        string checklistContent = "";
        if (Application.platform == RuntimePlatform.WindowsEditor)
                checklistContent = getFileContent(checklistFile);
        if (Application.platform == RuntimePlatform.Android)
            checklistContent = getPhoneFileContent(checklistFile);
        using (XmlReader xmlReader = XmlReader.Create(new StringReader(checklistContent)))
        {
            string tag = "";
            int counter = 0;
            GameObject oldElement = null;
            GameObject currElement = null;
            string description = null;
            int stepCounter = 0;
            GameObject currVideoObj = null;
            while (xmlReader.Read())
            {
                string val = xmlReader.Value.Replace("\t", "").Replace("\n", "");
                if (xmlReader.NodeType == XmlNodeType.Element)
                {
                    tag = xmlReader.Name;
                    if (tag == "Step")
                    {
                        counter++;
                        currElement = createChecklistElement();
                        currElement.transform.SetParent(checkList.transform);
                        currVideoObj = currElement.transform.Find("Panel").Find("VideoButton").gameObject;
                        currVideoObj.SetActive(false);
                        currElement.name = "Step " + stepCounter;
                        description = "";
                        stepCounter++;
                    }
                }
                
                if (val != "" && currElement != null && xmlReader.NodeType == XmlNodeType.Text)
                {
                    
                    if (tag == "Überschrift")
                    {
                        Transform head = currElement.transform.Find("Panel").Find("Head");
                        if (head != null)
                        {
                            ((Text)head.gameObject.GetComponent<Text>()).text = val;
                        }
                    }
                    if (tag == "Text")
                    {
                        description += val + "\n";
                    }
                    if (tag == "Video")
                    {
                        if (currVideoObj != null)
                        {
                            currVideoObj.SetActive(true);
                            VideoButton videoButtonScript = currVideoObj.GetComponent<VideoButton>();
                            if (videoButtonScript != null)
                            {
                                videoButtonScript.machineName = machineName;
                                videoButtonScript.videoName = val;
                            }
                        }
                    }
                }
                if (xmlReader.NodeType == XmlNodeType.EndElement && xmlReader.Name == "Step" && currElement != null)
                {
                    Transform desc = currElement.transform.Find("Description");
                    if (desc != null && description != null)
                    {
                        ((Text)desc.gameObject.GetComponent<Text>()).text = description;
                    }
                    if (oldElement != null)
                    {
                        Transform oldDesc = oldElement.transform.Find("Description");
                        if (oldDesc)
                        {
                            Text oldDescText = oldDesc.gameObject.GetComponent<Text>();
                            Vector3 pos = oldElement.transform.localPosition;
                            Canvas.ForceUpdateCanvases();
                            float prefHeight = LayoutUtility.GetPreferredHeight(oldDescText.rectTransform);
                            float yPos = oldDesc.transform.localPosition.y - prefHeight;//oldDescText.rectTransform.sizeDelta.y;
                            currElement.transform.localPosition = new Vector3(pos.x, yPos, pos.z);
                        }
                    }
                    oldElement = currElement;
                    currElement.SetActive(true);
                }
            }
        }
    }

    private GameObject createChecklistElement()
    {
            return Instantiate(checklistTemplate);
    }

    private void createContactText()
    {
        if (contactText == null)
        {
            string fileContent = "";
            if(Application.platform == RuntimePlatform.WindowsEditor)
                fileContent = getFileContent(contactFile);
            if(Application.platform == RuntimePlatform.Android)
                fileContent = getPhoneFileContent(contactFile);
            string text = "";
            using (XmlReader xmlReader = XmlReader.Create(new StringReader(fileContent)))
            {
                string tag = "";
                while (xmlReader.Read())
                {
                    if (xmlReader.NodeType == XmlNodeType.Element)
                    {
                        tag = xmlReader.Name;
                    }
                    string val = xmlReader.Value.Replace("\t", "").Replace("\n", "");
                    if (val != "")
                    {
                        if (tag == "Name")
                        {
                            text += "<b>";
                        }
                        text += val;
                        if (tag == "Name")
                        {
                            text += "</b>";
                        }
                    }
                    if (xmlReader.NodeType == XmlNodeType.EndElement)
                    {
                        text += "\n";
                        if (tag == "Person")
                            text += "\n";
                    }
                }
            }
            contactText = text;
        }
    }

    private void createMachineInfoText()
    {
        if(machineInfo == null)
        {
            string infoContent = "";
            if(Application.platform == RuntimePlatform.WindowsEditor)
                infoContent = getFileContent(descriptionFile);
            if(Application.platform == RuntimePlatform.Android)
                infoContent = getPhoneFileContent(descriptionFile);
            string text = "";
            using (XmlReader xmlReader = XmlReader.Create(new StringReader(infoContent)))
            {
                string tag = "";
                while (xmlReader.Read())
                {
                    if (xmlReader.NodeType == XmlNodeType.Element)
                    {
                        tag = xmlReader.Name;
                    }
                    string val = xmlReader.Value.Replace("\t", "");
                    if (val != "")
                    {
                        text += val;
                    }
                }
            }
            machineInfo = text;
        }
    }

    public void DisplayKontakt()
    {
        if(contactElement != null)
        {
            resetContentPos();
            scrollPane.gameObject.SetActive(true);
            contactElement.text = contactText;
            contactElement.gameObject.SetActive(true);

        }
    }

    public void DisplayInformation()
    {
        if(contactElement != null)
        {
            resetContentPos();
            scrollPane.gameObject.SetActive(true);
            contactElement.text = machineInfo;
            contactElement.gameObject.SetActive(true);
        }
    }

    public string getPhoneFileContent(string fileName)
    {

        WWW www = new WWW(fileName);
        while(!www.isDone)
        { }
        string ret;
        if (www.error == null)
        {
            ret = Encoding.UTF8.GetString(www.bytes).Trim();
        }
        else
        {
            ret = "";
        }
         return ret;
    }

    public string getFileContent(string fileName)
    {
        StreamReader reader = new StreamReader(fileName, Encoding.Default);
        string fileContent = "";
        using (reader)
        {
            string line = null;
            do
            {
                line = reader.ReadLine();
                fileContent += line;
            } while (line != null);
        }
        reader.Close();
        return fileContent;
    }
}
