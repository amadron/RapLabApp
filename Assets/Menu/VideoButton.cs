﻿using UnityEngine;
using System.Collections;

public class VideoButton : MonoBehaviour {

    public string videoName = null;
    public string machineName = null;
    public string fileExtension = ".mp4";
    // Use this for initialization
    void Start () {
	    
	}

    public void PlayVideo()
    {
        if (videoName != null && machineName != null)
        {
            string video = machineName + "/" + videoName + fileExtension;
            Debug.Log("Play Video:" + video);
            Handheld.PlayFullScreenMovie(video);
        }
    }

	// Update is called once per frame
	void Update () {
	
	}
}
